# User Updates

**State:** alpha

`user-updates` is a project that allows a user to automate updating of tools
and scripts in their home directory. The update runs in 3 different stages.
They are:

1. pre - used to run commands before modifying files
1. additions/deletions - this is to change files
1. post - used to run commands after modifying files

**Example:**

The tool can be run by `user-updates apply` which will use and flows you have
defined and then make changes to your system. Here is mine:
<https://gitlab.com/perobertson/dotfiles/-/blob/main/user-update-flow.yml>

## Installation

```bash
cargo install --git "https://gitlab.com/perobertson-tools/user-updates.git"
```

### Updating

To update the `user-updates` tool you can rerun the installation command.
This process can also be automated by creating scheduled tasks to do so.

**Systemd:**

Systemd needs a service definition created and then the service needs to be
started. After that is done the service will automatically update the tool when
changes are detected.

```bash
# Displays to stdout instead of writing the service file
user-updates bootstrap systemd-update-service --stdout
# Creates ~/.config/systemd/user/user-updates-update.service
user-updates bootstrap systemd-update-service

# Tell systemd to reload all the config files to discover the new service
systemctl --user daemon-reload
# Enable the service so it runs at boot and also start it now
systemctl --user enable --now user-updates-update.service
```

Logs can be inspected by using `journalctl`

```bash
journalctl --user -u user-updates-update
```

## Usage

Once the tools is installed to a location on your `PATH`, you can then use
its built in documentation to find out how the tool can be used.

```bash
# Use the help flag to see the most up to date documentation
user-updates --help
# The main entry point for applying changes to the system
user-updates apply --help
```

### Automated usage

Similar to the auto updating of the tool, the tool itself can be run on a
schedule to automate the updating of files and changes for the user.

**Systemd:**

Systemd needs a service definition created and then the service needs to be
started. After that is done the service will automatically run the tool.

```bash
# Displays to stdout instead of writing the service file
user-updates bootstrap systemd-service --stdout
# Creates ~/.config/systemd/user/user-updates.service
user-updates bootstrap systemd-service

# Tell systemd to reload all the config files to discover the new service
systemctl --user daemon-reload
# Enable the service so it runs at boot and also start it now
systemctl --user enable --now user-updates.service
```

Logs can be inspected by using `journalctl`

```bash
journalctl --user -u user-updates
```

## Configuration

**State:** alpha

The `user-updates` tool will read the configuration file to learn which
repositories need to be downloaded to run their configuration scripts.

File: `~/.config/user-updates/settings.yml`

```yml
version: "1"

# Repos to clone into `${XDG_DATA_HOME:-$HOME/.local/share}/user-updates/repos`
# The file `user-update-flow.yml` will be read from the root of the
# repository and will follow the config.yml schema.
# The paths in the config will be relative to the root of its repository.
repos:
    - url: https://gitlab.com/perobertson/user-updates-contents.git
    - url: https://gitlab.com/perobertson/user-updates-private.git
      privatekey: /home/user/.ssh/id_ed25519
```

In the remote repository there should be a file called `user-updates-flow.yml`
at the root of the repository. This file will define how the user updates
are executed. There are three stages to an update.

1. pre - used to run commands before modifying files
1. additions/deletions - this is to change files
1. post - used to run commands after modifying files

Schema: `user-update-flow.yml`

```yml
version: "1"

pre:
    cmds:
        - systemctl --user stop my-unit.service
additions:
    files:
        - name: XDG_CONFIG_HOME/systemd/user/my-new-unit.timer
          mode: 0o600
    templates:
        - name: XDG_CONFIG_HOME/systemd/user/my-new-unit.service.j2
          mode: 0o600
deletions:
    files:
        - XDG_CONFIG_HOME/systemd/user/my-unit.service
post:
    cmds:
        - systemctl --user enable my-new-unit.timer
        - systemctl --user start my-new-unit.timer
```

`additions.templates.name` will strip the `.j2` suffix if it exists when writing
the file.

`additions.files.name`, `additions.templates.name`, and `deletions.files`
supports the XDG base directory specification.
To write files to one of those directories, start the name with one of
the variable names followed by a forward slash (`/`).

- XDG_CACHE_HOME
- XDG_CONFIG_HOME
- XDG_DATA_HOME
- XDG_RUNTIME_DIR
- XDG_STATE_HOME

## Templating

<https://keats.github.io/tera/> is used for templating.
Which has a lot of similarities to Jinja2.

These are the context variables that are available:

- cache_dir
- cargo_install_root
- config_dir
- data_dir
- home_dir
- runtime_dir
- state_dir
