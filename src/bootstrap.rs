use std::{
    env,
    fs::{self, create_dir_all},
};

use anyhow::Result;
use directories::BaseDirs;
use log::debug;

/// Return the default systemd service definition for running the tool.
pub(crate) fn systemd_service(stdout: bool) -> Result<()> {
    let environment_arg = match env::var("CARGO_INSTALL_ROOT") {
        Ok(val) => format!("Environment=CARGO_INSTALL_ROOT={}", val),
        Err(_) => "".to_string(),
    };
    let bin_root = match env::var("CARGO_INSTALL_ROOT") {
        Ok(val) => format!("{}/bin", val),
        Err(_) => "%h/.cargo/bin".to_string(),
    };
    let service_unit = format!(
        r#"
[Unit]
Description=Update user scripts and tools
After=default.target

[Service]
Type=simple
{environment_arg}
ExecStart={bin_root}/user-updates apply

[Install]
# https://www.freedesktop.org/software/systemd/man/systemd.special.html#default.target1
# The main target for user sessions
WantedBy=default.target
"#
    );
    write_service(&service_unit, stdout, "user-updates.service")
}

/// Return the default systemd service definition for updating the tool.
pub(crate) fn systemd_update_service(stdout: bool) -> Result<()> {
    let environment_arg = match env::var("CARGO_INSTALL_ROOT") {
        Ok(val) => format!("Environment=CARGO_INSTALL_ROOT={}", val),
        Err(_) => "".to_string(),
    };
    let service_unit = format!(
        r#"
[Unit]
Description=Update user-updates
ConditionACPower=true
After=default.target

[Service]
Type=simple
{environment_arg}
ExecStart=cargo install --git "https://gitlab.com/perobertson-tools/user-updates.git"

[Install]
# https://www.freedesktop.org/software/systemd/man/systemd.special.html#default.target1
# The main target for user sessions
WantedBy=default.target
"#
    );
    write_service(&service_unit, stdout, "user-updates-update.service")
}

/// Write the service to ~/.config/systemd/user/ with the specified filename
fn write_service(contents: &str, stdout: bool, filename: &str) -> Result<()> {
    let contents = contents.trim();
    if stdout {
        println!("{}", contents)
    } else if let Some(base_dirs) = BaseDirs::new() {
        let systemd_dir = base_dirs.config_dir().join("systemd").join("user");
        create_dir_all(&systemd_dir)?;
        let service = systemd_dir.join(filename);
        fs::write(&service, contents)?;
        debug!("wrote service to {}", service.display());
    }
    Ok(())
}
