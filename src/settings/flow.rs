use std::{
    fs::{self, create_dir_all, read_to_string, File},
    io::Write,
    os::unix::prelude::PermissionsExt,
    path::{Path, PathBuf},
    process::Command,
};

use anyhow::{anyhow, Result};
use log::{info, warn};
use serde::Deserialize;
use tera::{Context as TeraContext, Tera};

use crate::context::Context;

fn determine_destination(context: &Context, file_name: &String) -> Option<PathBuf> {
    // https://wiki.archlinux.org/title/XDG_Base_Directory
    if file_name.starts_with("XDG_CACHE_HOME/") {
        let destination_file_name = file_name.replacen("XDG_CACHE_HOME/", "", 1);
        return Some(PathBuf::from(&context.cache_dir).join(destination_file_name));
    } else if file_name.starts_with("XDG_CONFIG_HOME/") {
        let destination_file_name = file_name.replacen("XDG_CONFIG_HOME/", "", 1);
        return Some(PathBuf::from(&context.config_dir).join(destination_file_name));
    } else if file_name.starts_with("XDG_DATA_HOME/") {
        let destination_file_name = file_name.replacen("XDG_DATA_HOME/", "", 1);
        return Some(PathBuf::from(&context.data_dir).join(destination_file_name));
    } else if file_name.starts_with("XDG_RUNTIME_DIR/") {
        if let Some(runtime_dir) = &context.runtime_dir {
            let destination_file_name = file_name.replacen("XDG_RUNTIME_DIR/", "", 1);
            return Some(PathBuf::from(runtime_dir).join(destination_file_name));
        }
        warn!(
            "XDG_RUNTIME_DIR is not set. Cannot determine destination for file: {}",
            file_name
        );
        return None;
    } else if file_name.starts_with("XDG_STATE_HOME/") {
        if let Some(state_dir) = &context.state_dir {
            let destination_file_name = file_name.replacen("XDG_STATE_HOME/", "", 1);
            return Some(PathBuf::from(&state_dir).join(destination_file_name));
        }
        warn!(
            "XDG_STATE_HOME is not set. Cannot determine destination for file: {}",
            file_name
        );
        return None;
    }
    Some(PathBuf::from(&context.home_dir).join(file_name))
}

fn run_cmd(cmd: &str) -> Result<()> {
    info!("running cmd: {}", cmd);
    let status = Command::new("sh").arg("-c").arg(cmd).status()?;
    match status.code() {
        Some(status) => match status {
            0 => Ok(()),
            code => {
                let msg = format!("running cmd: {}; exited with: {}", cmd, code);
                Err(anyhow!(msg))
            }
        },
        None => {
            let msg = format!("no exit code returned from running cmd: {}", cmd);
            Err(anyhow!(msg))
        }
    }
}

pub struct Flow {
    /// The directory where the repository was cloned to that defines this flow
    repo_dir: String,

    /// The raw data read from `user-update-flow.yml`
    raw: RawFlow,
}
impl Flow {
    /// Copy a file from the repository to the host
    ///
    /// - `files_dir` - `<settings_repo_dir>/files`
    /// - `flow_file` - the file defined in the flow
    fn addition(&self, context: &Context, files_dir: &Path, flow_file: &RawFlowFile) -> Result<()> {
        let file = files_dir.join(&flow_file.name);
        let destination = match determine_destination(context, &flow_file.name) {
            Some(destination) => destination,
            None => {
                return Err(anyhow!(
                    "could not determine destination for file: {}",
                    file.display()
                ));
            }
        };
        info!(
            "copying from {} to {}",
            file.display(),
            destination.display()
        );
        if file == destination {
            return Err(anyhow!(
                 "source and destination cannot be the same file. It will cause the file to be truncated",
            ));
        }
        if destination.is_symlink() {
            warn!("removing symlink at destination before copying");
            fs::remove_file(&destination)?;
        }
        // On the first run the parent directories may not exist
        if let Some(parent) = destination.parent() {
            create_dir_all(parent)?;
        }
        fs::copy(&file, &destination)?;

        let metadata = destination.metadata()?;
        let mut permissions = metadata.permissions();
        permissions.set_mode(flow_file.mode);
        info!(
            "changing mode of {} to {:o}",
            destination.display(),
            permissions.mode()
        );
        fs::set_permissions(&destination, permissions)?;
        Ok(())
    }

    /// Read will read the `user-update-flow.yml` file
    pub fn read(path: String) -> Result<Self> {
        let path = Path::new(&path);
        let raw = RawFlow::read(path).unwrap();
        let repo_dir = path.parent().unwrap().to_str().unwrap().to_string();
        let flow = Flow { raw, repo_dir };
        Ok(flow)
    }

    pub fn run(context: &Context, flows: Vec<Flow>) -> Result<()> {
        for flow in flows.iter() {
            flow.run_pre()?;
        }
        for flow in flows.iter() {
            flow.run_additions(context)?;
        }
        for flow in flows.iter() {
            flow.run_deletions(context)?;
        }
        for flow in flows.iter() {
            flow.run_post()?;
        }
        Ok(())
    }

    fn run_pre(&self) -> Result<()> {
        for cmd in self.raw.pre.cmds.iter() {
            run_cmd(cmd)?;
        }
        Ok(())
    }

    fn run_additions(&self, context: &Context) -> Result<()> {
        let repo_dir = PathBuf::from(&self.repo_dir);
        let files_dir = repo_dir.join("files");
        let templates_dir = repo_dir.join("templates");

        for f in self.raw.additions.files.iter() {
            self.addition(context, &files_dir, f)?;
        }
        for t in self.raw.additions.templates.iter() {
            self.template(context, &templates_dir, t)?;
        }
        Ok(())
    }

    fn run_deletions(&self, context: &Context) -> Result<()> {
        for f in self.raw.deletions.files.iter() {
            let file_to_delete = match determine_destination(context, f) {
                Some(destination) => destination,
                None => {
                    return Err(anyhow!("could not determine destination for file: {}", f));
                }
            };
            if !file_to_delete.exists() {
                continue;
            }
            info!("Removing {}", file_to_delete.display());
            if file_to_delete.is_dir() {
                fs::remove_dir(file_to_delete)?;
            } else {
                fs::remove_file(file_to_delete)?;
            }
        }
        Ok(())
    }

    fn run_post(&self) -> Result<()> {
        for cmd in self.raw.post.cmds.iter() {
            run_cmd(cmd)?;
        }
        Ok(())
    }

    fn template(
        &self,
        context: &Context,
        templates_dir: &Path,
        template_file: &RawFlowTemplate,
    ) -> Result<()> {
        let file = templates_dir.join(&template_file.name);
        let destination_file_name = match template_file.name.strip_suffix(".j2") {
            Some(v) => v,
            None => &template_file.name,
        }
        .to_string();
        let destination = match determine_destination(context, &destination_file_name) {
            Some(destination) => destination,
            None => {
                return Err(anyhow!(
                    "could not determine destination for file: {}",
                    destination_file_name
                ));
            }
        };
        info!(
            "templating from {} to {}",
            file.display(),
            destination.display()
        );
        if file == destination {
            return Err(anyhow!(
                 "source and destination cannot be the same file. It will cause the file to be truncated",
            ));
        }
        if destination.is_symlink() {
            warn!("removing symlink at destination before templating");
            fs::remove_file(&destination)?;
        }
        // On the first run the parent directories may not exist
        if let Some(parent) = destination.parent() {
            create_dir_all(parent)?;
        }

        // install template
        let input = read_to_string(file)?;
        let tera_context = TeraContext::from_serialize(context)?;
        let result = Tera::one_off(&input, &tera_context, false)?;
        let mut destination_file = File::create(&destination)?;
        destination_file.write_all(result.as_bytes())?;

        // set permissions on newly templated file
        let metadata = destination.metadata()?;
        let mut permissions = metadata.permissions();
        permissions.set_mode(template_file.mode);
        info!(
            "changing mode of {} to {:o}",
            destination.display(),
            permissions.mode()
        );
        fs::set_permissions(&destination, permissions)?;
        Ok(())
    }
}

#[derive(Deserialize)]
struct RawFlow {
    pre: RawFlowPre,
    additions: RawFlowAdditions,
    deletions: RawFlowDeletions,
    post: RawFlowPost,
}
impl RawFlow {
    /// Read will read the `user-update-flow.yml` file
    fn read(path: &Path) -> Result<Self> {
        let s = read_to_string(path)?;
        let flow: Self = serde_yml::from_str(&s)?;
        Ok(flow)
    }
}

#[derive(Deserialize)]
struct RawFlowPre {
    cmds: Vec<String>,
}

#[derive(Deserialize)]
struct RawFlowAdditions {
    files: Vec<RawFlowFile>,
    templates: Vec<RawFlowTemplate>,
}

#[derive(Deserialize)]
struct RawFlowDeletions {
    files: Vec<String>,
}

#[derive(Deserialize)]
struct RawFlowPost {
    cmds: Vec<String>,
}

#[derive(Deserialize)]
struct RawFlowFile {
    name: String,
    mode: u32,
}

#[derive(Deserialize)]
struct RawFlowTemplate {
    name: String,
    mode: u32,
}

#[cfg(test)]
mod tests {
    use anyhow::Result;
    use std::path::PathBuf;
    use test_case::test_case;

    use crate::context::Context;

    use super::determine_destination;

    #[test_case("README.md", "README.md"; "normal file")]
    #[test_case("XDG_CACHE_HOME/README.md", ".cache/README.md"; "XDG_CACHE_HOME")]
    #[test_case("XDG_CONFIG_HOME/README.md", ".config/README.md"; "XDG_CONFIG_HOME")]
    #[test_case("XDG_DATA_HOME/README.md", ".local/share/README.md"; "XDG_DATA_HOME")]
    #[test_case("XDG_STATE_HOME/README.md", ".local/state/README.md"; "XDG_STATE_HOME")]
    fn test_determine_destination_in_home_dir(file_name: &str, expected: &str) -> Result<()> {
        let context = Context::new()?;
        let file_name = file_name.to_string();
        let result = determine_destination(&context, &file_name).unwrap();

        assert_eq!(PathBuf::from(context.home_dir).join(expected), result);
        Ok(())
    }

    #[test_case("XDG_RUNTIME_DIR/README.md", "README.md"; "XDG_RUNTIME_DIR")]
    fn test_determine_destination_in_runtime_dir(file_name: &str, expected: &str) -> Result<()> {
        let context = Context::new()?;
        let file_name = file_name.to_string();
        let result = determine_destination(&context, &file_name);

        if let Some(runtime_dir) = context.runtime_dir {
            assert_eq!(PathBuf::from(runtime_dir).join(expected), result.unwrap());
        } else {
            assert_eq!(None, result);
        }
        Ok(())
    }
}
