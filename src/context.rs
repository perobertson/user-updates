use anyhow::{Context as ErrContext, Result};
use directories::BaseDirs;
use serde::Serialize;
use std::env;

#[derive(Serialize)]
pub struct Context {
    pub cache_dir: String,
    pub cargo_install_root: Option<String>,
    pub config_dir: String,
    pub data_dir: String,
    pub home_dir: String,

    // only available on linux
    pub runtime_dir: Option<String>,
    pub state_dir: Option<String>,
}
impl Context {
    pub fn new() -> Result<Self> {
        let cargo_install_root = match env::var("CARGO_INSTALL_ROOT") {
            Ok(val) => Some(val),
            Err(_) => None,
        };

        let base = BaseDirs::new().with_context(|| "home directory could not be determined")?;
        let cache_dir = base.cache_dir().display().to_string();
        let config_dir = base.config_dir().display().to_string();
        let data_dir = base.data_dir().display().to_string();
        let home_dir = base.home_dir().display().to_string();
        let runtime_dir = base.runtime_dir().map(|d| d.display().to_string());
        let state_dir = base.state_dir().map(|d| d.display().to_string());

        let ctx = Context {
            cache_dir,
            cargo_install_root,
            config_dir,
            data_dir,
            home_dir,
            runtime_dir,
            state_dir,
        };

        Ok(ctx)
    }
}
