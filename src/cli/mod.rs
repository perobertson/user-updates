use std::io;

use anyhow::Result;
use clap::{ArgAction, Command, CommandFactory, Parser, Subcommand};
use clap_complete::{generate, Generator, Shell};
use log::warn;

mod config;

use crate::bootstrap::{systemd_service, systemd_update_service};

/// Tool for performing updates in the users home directory
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub(super) struct CliArgs {
    #[command(subcommand)]
    command: Option<CliCmd>,
}
impl CliArgs {
    pub(super) fn run(&self) -> Result<()> {
        match &self.command {
            Some(cmd) => cmd.run(),
            None => {
                // This section is when there are no commands passed in
                warn!("Applying changes to the system from the root command is deprecated. Please use the apply subcommand instead");
                super::apply()
            }
        }
    }
}

#[derive(Subcommand)]
enum CliCmd {
    Apply(ApplyArgs),
    Bootstrap(BootstrapArgs),
    Completion(CompletionArgs),
    Config(config::ConfigArgs),
}
impl CliCmd {
    fn run(&self) -> Result<()> {
        match &self {
            Self::Apply(args) => args.run(),
            Self::Bootstrap(args) => args.run(),
            Self::Completion(args) => args.run(),
            Self::Config(args) => args.run(),
        }
    }
}

/// Apply changes to the system as defined by the user update flows.
#[derive(clap::Args)]
struct ApplyArgs {}
impl ApplyArgs {
    fn run(&self) -> Result<()> {
        super::apply()
    }
}

/// Commands for setting up continual updates
#[derive(clap::Args)]
struct BootstrapArgs {
    #[command(subcommand)]
    command: BootstrapCmd,
}
impl BootstrapArgs {
    fn run(&self) -> Result<()> {
        self.command.run()
    }
}

#[derive(Subcommand)]
enum BootstrapCmd {
    SystemdService(BootstrapSystemdServiceArgs),
    SystemdUpdateService(BootstrapSystemdUpdateServiceArgs),
}
impl BootstrapCmd {
    fn run(&self) -> Result<()> {
        match self {
            BootstrapCmd::SystemdService(args) => args.run()?,
            BootstrapCmd::SystemdUpdateService(args) => args.run()?,
        }
        Ok(())
    }
}

/// Output a config for running the user-updates.service.
#[derive(clap::Args)]
struct BootstrapSystemdServiceArgs {
    /// Write to stdout instead of writing to the users systemd config
    #[clap(long, action=ArgAction::SetTrue)]
    stdout: bool,
}
impl BootstrapSystemdServiceArgs {
    fn run(&self) -> Result<()> {
        systemd_service(self.stdout)
    }
}

/// Output a config for running the user-updates-update.service.
#[derive(clap::Args)]
struct BootstrapSystemdUpdateServiceArgs {
    /// Write to stdout instead of writing to the users systemd config
    #[clap(long, action=ArgAction::SetTrue)]
    stdout: bool,
}
impl BootstrapSystemdUpdateServiceArgs {
    fn run(&self) -> Result<()> {
        systemd_update_service(self.stdout)
    }
}

/// Generates completions for a given shell
#[derive(clap::Args)]
struct CompletionArgs {
    // The kind of shell to generate completions for
    #[arg(value_enum)]
    shell: Shell,
}
impl CompletionArgs {
    /// Print completions for a given shell
    fn run(&self) -> Result<()> {
        let mut cmd = CliArgs::command();
        print_completions(self.shell, &mut cmd);
        Ok(())
    }
}
fn print_completions<G: Generator>(gen: G, cmd: &mut Command) {
    generate(gen, cmd, cmd.get_name().to_string(), &mut io::stdout());
}
