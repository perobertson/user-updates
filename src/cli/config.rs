use std::fs::{self, create_dir_all};

use anyhow::Result;
use clap::Subcommand;

use crate::{project_dirs, settings::Settings};

/// Commands for working with the user-updates settings
#[derive(clap::Args)]
pub(super) struct ConfigArgs {
    #[command(subcommand)]
    command: ConfigCmd,
}
impl ConfigArgs {
    pub(super) fn run(&self) -> Result<()> {
        self.command.run()
    }
}

#[derive(Subcommand)]
enum ConfigCmd {
    AddRepo(ConfigAddRepoArgs),
    Show(ConfigShowArgs),
    Validate(ConfigValidateArgs),
}
impl ConfigCmd {
    fn run(&self) -> Result<()> {
        match self {
            Self::AddRepo(args) => args.run(),
            Self::Show(args) => args.run(),
            Self::Validate(args) => args.run(),
        }
    }
}

/// Add a repo to the current config file contents
#[derive(clap::Args)]
struct ConfigAddRepoArgs {
    /// URL of the git repo to add
    url: String,
}
impl ConfigAddRepoArgs {
    fn run(&self) -> Result<()> {
        let mut settings = Settings::load()?;
        settings.add_repo(&self.url)?;
        settings.save()?;
        Ok(())
    }
}

/// Show the current config file contents
#[derive(clap::Args)]
struct ConfigShowArgs {}
impl ConfigShowArgs {
    fn run(&self) -> Result<()> {
        let dirs = project_dirs()?;
        let config_dir = dirs.config_dir();
        create_dir_all(config_dir)?;
        let settings = config_dir.join("settings.yml");
        let contents = fs::read_to_string(settings)?;
        println!("{}", contents);
        Ok(())
    }
}

/// Validate the current config file contents
#[derive(clap::Args)]
struct ConfigValidateArgs {}
impl ConfigValidateArgs {
    fn run(&self) -> Result<()> {
        todo!("implement validation")
    }
}
