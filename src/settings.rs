use std::{
    fs::{self, create_dir_all},
    path::{Path, PathBuf},
    vec,
};

use anyhow::{anyhow, Context as ErrContext, Result};
use git2::{
    build::{CheckoutBuilder, RepoBuilder},
    Cred, FetchOptions, RemoteCallbacks, Repository,
};
use log::{debug, info, warn};
use serde::{Deserialize, Serialize};
use url::Url;

mod flow;

use crate::{context::Context, project_dirs};

use self::flow::Flow;

/// File: `~/.config/user-updates/settings.yml`
///
/// Repos to clone into `${XDG_DATA_HOME:-$HOME/.local/share}/user-updates/repos`
/// The file `user-update-config.yml` will be read from the root of the
/// repository and will follow the config.yml schema.
/// The paths in the config will be relative to the root of its repository.
#[derive(Deserialize, Serialize)]
pub struct Settings {
    repos: Vec<SettingsRepo>,
    version: String,
}
impl Settings {
    /// Add a repository to the settings
    pub(crate) fn add_repo(&mut self, url: &str) -> Result<()> {
        Url::parse(url).with_context(|| "url is not a valid url")?;
        for repo in self.repos.iter() {
            if url == repo.url {
                warn!("{} is already added", url);
                return Ok(());
            }
        }
        self.repos.push(SettingsRepo {
            url: url.to_string(),
            privatekey: None,
        });
        Ok(())
    }
    /// Aply the flows definied by the settings repos.
    /// All the steps in the `pre` stage are run before the `additions` stage.
    /// `additions` are run before the `deletions`.
    /// `deletions` are run before the `post`.
    pub fn apply(&self, context: &Context) -> Result<()> {
        let flows = self.flows(context)?;
        Flow::run(context, flows)
    }
    fn flows(&self, context: &Context) -> Result<Vec<Flow>> {
        let flows: Vec<Flow> = self
            .repos
            .iter()
            .map(|r| r.load(context).unwrap())
            .collect();
        Ok(flows)
    }
    pub fn default() -> Self {
        Settings {
            repos: vec![],
            version: '1'.to_string(),
        }
    }
    // Load settings from the config file
    pub fn load() -> Result<Self> {
        let dirs = project_dirs()?;
        let config_dir = dirs.config_dir();
        create_dir_all(config_dir)?;
        let path = config_dir.join("settings.yml");
        if path.exists() {
            debug!("loading settings from {}", path.display());
            Self::read(&path)
        } else {
            debug!("config does not exist. using default settings.");
            Ok(Self::default())
        }
    }
    /// Read will read the `settings.yml` file
    pub fn read(path: &Path) -> Result<Self> {
        let s = fs::read_to_string(path)?;
        let settings: Settings = serde_yml::from_str(&s)?;
        if settings.version != "1" {
            Err(anyhow!("unknown version in settings.yml"))
        } else {
            Ok(settings)
        }
    }
    /// Save the current settings to the config file
    pub fn save(&self) -> Result<()> {
        let dirs = project_dirs()?;
        let config_dir = dirs.config_dir();
        create_dir_all(config_dir)?;
        let path = config_dir.join("settings.yml");
        debug!("saving settings to {}", path.display());
        let contents = serde_yml::to_string(self)?;
        fs::write(path, contents)?;
        Ok(())
    }
    /// Update all of the settings repos by pulling the latest commits.
    pub fn update(&self, context: &Context) -> Result<()> {
        for repo in self.repos.iter() {
            repo.get_latest(context)?;
        }
        Ok(())
    }
}

#[derive(Deserialize, Serialize)]
struct SettingsRepo {
    url: String,
    privatekey: Option<PathBuf>,
}
impl SettingsRepo {
    fn clone_repo(&self, repo_dir: &Path) -> Result<()> {
        // Prepare builder.
        let mut builder = RepoBuilder::new();

        if let Some(ref privatekey) = self.privatekey {
            // Prepare callbacks.
            let mut callbacks = RemoteCallbacks::new();
            callbacks.credentials(|_url, username_from_url, _allowed_types| {
                Cred::ssh_key(username_from_url.unwrap(), None, privatekey, None)
            });
            // Prepare fetch options.
            let mut fo = FetchOptions::new();
            fo.remote_callbacks(callbacks);
            // Use the fetch options
            builder.fetch_options(fo);
        }

        // Clone the project.
        match builder.clone(&self.url, repo_dir) {
            Ok(r) => {
                info!("cloned into: {}", r.path().display());
                Ok(())
            }
            Err(e) => {
                let msg = format!("failed to clone into {}. Cause: {}", repo_dir.display(), e);
                Err(anyhow!(msg))
            }
        }
    }

    /// Clone or pull the latest changes for the settings repo
    fn get_latest(&self, context: &Context) -> Result<()> {
        let repo_dir = self.repo_dir(context)?;
        if repo_dir.exists() {
            self.pull_repo(&repo_dir)
        } else {
            self.clone_repo(&repo_dir)
        }
    }

    /// Parse the `user-updates-flow.yml` and return a Flow
    fn load(&self, context: &Context) -> Result<Flow> {
        let flow_config = self.repo_dir(context)?.join("user-update-flow.yml");
        let config_file = flow_config.to_str().unwrap().to_string();
        let flow = Flow::read(config_file)?;
        Ok(flow)
    }

    /// https://github.com/rust-lang/git2-rs/tree/master/examples
    fn pull_repo(&self, repo_dir: &PathBuf) -> Result<()> {
        // Read the repository metadata
        let repo = Repository::open(repo_dir)?;

        // Fetch the latest changes for origin/main
        let mut remote = repo.find_remote("origin")?;

        if let Some(ref privatekey) = self.privatekey {
            // Prepare callbacks.
            let mut callbacks = RemoteCallbacks::new();
            callbacks.credentials(|_url, username_from_url, _allowed_types| {
                Cred::ssh_key(username_from_url.unwrap(), None, privatekey, None)
            });
            // Prepare fetch options.
            let mut fo = FetchOptions::new();
            fo.remote_callbacks(callbacks);

            match remote.fetch(&["main"], Some(&mut fo), None) {
                Ok(_) => {}
                Err(e) => {
                    let msg = format!(
                        "failed to fetch code for {}. Cause: {}",
                        repo_dir.display(),
                        e
                    );
                    return Err(anyhow!(msg));
                }
            };
        } else {
            match remote.fetch(&["main"], None, None) {
                Ok(_) => {}
                Err(e) => {
                    let msg = format!(
                        "failed to fetch code for {}. Cause: {}",
                        repo_dir.display(),
                        e
                    );
                    return Err(anyhow!(msg));
                }
            };
        }

        // This will update where main points to
        let fetch_head = repo.find_reference("FETCH_HEAD")?;
        let fetched_commit = repo.reference_to_annotated_commit(&fetch_head)?;
        let refname = format!("refs/heads/{}", "main");
        match repo.find_reference(&refname) {
            Ok(mut r) => {
                let msg = format!("Setting {} to id: {}", "main", fetched_commit.id());
                r.set_target(fetched_commit.id(), &msg)?;
            }
            Err(e) => {
                return Err(e.into());
            }
        }

        // This will change which commit is checked out
        // It is needed when "main" was not checked out in the first place
        // repo.set_head(fetched_commit.refname().unwrap())?;
        // repo.set_head_detached_from_annotated(fetched_commit)?;

        // This will change the repo to checkout a named reference
        repo.set_head(&refname)?;

        // This will reset the git index to a clean slate
        repo.checkout_head(Some(CheckoutBuilder::default().force()))?;

        Ok(())
    }

    /// The directory where the settings repo is stored.
    /// It uses the domain and path to help prevent conflicts.
    fn repo_dir(&self, context: &Context) -> Result<PathBuf> {
        let repo_dir = PathBuf::from(&context.data_dir)
            .join("user-updates")
            .join("repos");
        let url = Url::parse(&self.url)?;
        let domain = url.domain().unwrap();
        let segments = url.path_segments().map(|c| c.collect::<Vec<_>>()).unwrap();
        let mut code_path = repo_dir.join(domain);
        for segment in segments {
            code_path = code_path.join(segment);
        }
        Ok(code_path)
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use anyhow::Result;

    use super::Settings;

    #[test]
    fn test_default_settings() {
        let settings = Settings::default();
        assert_eq!('1'.to_string(), settings.version);
        assert_eq!(true, settings.repos.is_empty());
    }

    #[test]
    fn test_read() -> Result<()> {
        let path = Path::new("tests/fixtures/settings.yml");
        let settings = Settings::read(path)?;

        assert_eq!('1'.to_string(), settings.version);
        assert_eq!(1, settings.repos.len());
        let settings_repo = settings.repos.get(0).unwrap();
        assert_eq!(
            "https://gitlab.com/perobertson/user-updates-contents.git",
            settings_repo.url
        );
        Ok(())
    }
}
