use std::env;

use anyhow::{Context as ErrContext, Result};
use clap::Parser;
use directories::ProjectDirs;
use log::{debug, warn, LevelFilter};
use settings::Settings;
use systemd_journal_logger::{connected_to_journal, JournalLog};

mod bootstrap;
mod cli;
mod context;
mod settings;

use crate::cli::CliArgs;
use crate::context::Context;

/// Apply changes to the system by following the users configured flows
fn config_flow(context: &Context) -> Result<()> {
    let settings = match Settings::load() {
        Ok(settings) => settings,
        Err(e) => {
            warn!(
                "WARNING: failed to read settings, using defaults instead. Err: {}",
                e
            );
            Settings::default()
        }
    };
    settings.update(context)?;
    settings.apply(context)?;
    Ok(())
}

fn log_level() -> LevelFilter {
    match env::var_os("RUST_LOG") {
        Some(val) => {
            let val = val.to_ascii_lowercase();
            if val == "off" {
                LevelFilter::Off
            } else if val == "error" {
                LevelFilter::Error
            } else if val == "warn" {
                LevelFilter::Warn
            } else if val == "info" {
                LevelFilter::Info
            } else if val == "debug" {
                LevelFilter::Debug
            } else if val == "trace" {
                LevelFilter::Trace
            } else {
                LevelFilter::Info
            }
        }
        None => LevelFilter::Info,
    }
}

/// Apply changes to the system
fn apply() -> Result<()> {
    let context = Context::new()?;
    config_flow(&context)?;
    Ok(())
}

fn project_dirs() -> Result<ProjectDirs> {
    let dirs = ProjectDirs::from("com", "perobertson", env!("CARGO_PKG_NAME"))
        .with_context(|| "could not determine paths for project dirs")?;
    Ok(dirs)
}

fn main() -> Result<()> {
    if connected_to_journal() {
        JournalLog::new()
            .unwrap()
            .with_extra_fields(vec![("VERSION", env!("CARGO_PKG_VERSION"))])
            .install()?;
        let level = log_level();
        log::set_max_level(level);
    } else {
        let logger_env = env_logger::Env::default()
            .default_filter_or(format!("{}=info", env!("CARGO_CRATE_NAME")));
        env_logger::Builder::from_env(logger_env).init();
    }
    debug!("starting up");

    let args = CliArgs::parse();
    args.run()
}
